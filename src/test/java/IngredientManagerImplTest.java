import org.junit.Before;
import org.junit.Test;

import java.sql.SQLException;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

/**
 * Tests for IngredientManager class.
 * @author xliberov
 */
public class IngredientManagerImplTest {

    private IngredientManagerImpl manager;

    @Before
    public void setUp(){
        manager = new IngredientManagerImpl();
    }

    @Test
    public void addIngredientOK() {
        Long ingredientID = 1L;
        Ingredient ingredient = newIngredient(ingredientID,"Milk", IngredientUnit.ml);
        manager.addIngredient(ingredient);

        Ingredient result = manager.findIngredientByID(ingredientID);
        //loaded instance should be equal to the saved one
        assertThat("loaded ingredient differs from the saved one", result, is(equalTo(ingredient)));
        //but it should be another instance
        assertThat("loaded ingredient is the same instance", result, is(not(sameInstance(ingredient))));
        //Grave.equals() method may be broken, check properties' values
        assertDeepEquals(ingredient, result);
    }

    @Test(expected = IllegalArgumentException.class)
    public void addIngredientWithNull() throws IllegalArgumentException
    {
        manager.addIngredient(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void addIngredientDuplicateId() {
        Long ingredientID = 1L;
        Ingredient ingredientA = newIngredient(ingredientID,"Milk", IngredientUnit.ml);
        manager.addIngredient(ingredientA);
        manager.addIngredient(ingredientA);
    }

    @Test(expected = IllegalArgumentException.class)
    public void addIngredientNullId() {
        Ingredient ingredient = newIngredient(null,"Milk", IngredientUnit.ml);
        manager.addIngredient(ingredient);
    }

    @Test(expected = IllegalArgumentException.class)
    public void addIngredientInvalidName() {
        Long ingredientID = 1L;
        Ingredient ingredient = newIngredient(ingredientID, null, IngredientUnit.ml);
        manager.addIngredient(ingredient);
    }

    @Test(expected = IllegalArgumentException.class)
    public void addIngredientInvalidUnit() {
        Long ingredientID = 1L;
        Ingredient ingredient = newIngredient(ingredientID, "Milk", null);
        manager.addIngredient(ingredient);
    }

    @Test(expected = IllegalArgumentException.class)
    public void addIngredientNegativeId() {
        Long ingredientID = -1L;
        Ingredient ingredient = newIngredient(ingredientID, "Milk", IngredientUnit.ml);
        manager.addIngredient(ingredient);
    }

    @Test
    public void updateIngredientOK() {
        Long ingredientID = 1L;
        Ingredient ingredient = newIngredient(ingredientID, "Milk", IngredientUnit.ml);
        Ingredient updatedIngredient = newIngredient(ingredientID, "Whole milk", IngredientUnit.cup);
        manager.addIngredient(ingredient);
        manager.updateIngredient(updatedIngredient);

        Ingredient result = manager.findIngredientByID(ingredientID);
        //loaded instance should be equal to the saved one
        assertThat("loaded ingredient differs from the saved one", result, is(equalTo(updatedIngredient)));
        //but it should be another instance
        assertThat("loaded ingredient is the same instance", result, is(not(sameInstance(updatedIngredient))));
        //Grave.equals() method may be broken, check properties' values
        assertDeepEquals(updatedIngredient, result);
    }

    @Test(expected = IllegalArgumentException.class)
    public void updateIngredientEmptyStorage() {
        Long ingredientID = 1L;
        Ingredient updatedIngredient = newIngredient(ingredientID, "Whole milk", IngredientUnit.cup);
        manager.updateIngredient(updatedIngredient);
    }

    @Test(expected = IllegalArgumentException.class)
    public void updateIngredientWithNull() throws IllegalArgumentException
    {
        manager.updateIngredient(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void updateIngredientNullId() {
        Long ingredientID = 1L;
        Ingredient ingredient = newIngredient(ingredientID, "Milk", IngredientUnit.ml);
        Ingredient updatedIngredient = newIngredient(null, "Whole milk", IngredientUnit.cup);
        manager.addIngredient(ingredient);
        manager.updateIngredient(updatedIngredient);
    }

    @Test(expected = IllegalArgumentException.class)
    public void updateIngredientInvalidName() {
        Long ingredientID = 1L;
        Ingredient ingredient = newIngredient(ingredientID, "Milk", IngredientUnit.ml);
        Ingredient updatedIngredient = newIngredient(ingredientID, null, IngredientUnit.cup);
        manager.addIngredient(ingredient);
        manager.updateIngredient(updatedIngredient);
    }

    @Test(expected = IllegalArgumentException.class)
    public void updateIngredientInvalidUnit() {
        Long ingredientID = 1L;
        Ingredient ingredient = newIngredient(ingredientID, "Milk", IngredientUnit.ml);
        Ingredient updatedIngredient = newIngredient(ingredientID, "Whole milk", null);
        manager.addIngredient(ingredient);
        manager.updateIngredient(updatedIngredient);
    }

    @Test
    public void deleteIngredientOK() {
        Long ingredientID = 1L;
        Ingredient ingredient = newIngredient(ingredientID, "Milk", IngredientUnit.ml);
        manager.addIngredient(ingredient);
        manager.deleteIngredient(ingredient);

        Ingredient result = manager.findIngredientByID(ingredientID);

        assertNull("result should be null", result);
    }

    @Test(expected = IllegalArgumentException.class)
    public void deleteIngredientEmptyStorage() {
        Long ingredientID = 1L;
        Ingredient updatedIngredient = newIngredient(ingredientID, "Whole milk", IngredientUnit.cup);
        manager.deleteIngredient(updatedIngredient);
    }

    @Test(expected = IllegalArgumentException.class)
    public void deleteIngredientWithNull() throws IllegalArgumentException
    {
        manager.deleteIngredient(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void deleteIngredientNullId() {
        Long ingredientID = 1L;
        Ingredient ingredient = newIngredient(ingredientID, "Milk", IngredientUnit.ml);
        manager.addIngredient(ingredient);
        ingredient.setId(null);
        manager.deleteIngredient(ingredient);
    }

    @Test
    public void deleteIngredientNotInStorage() {
        Long ingredientID = 1L;
        Ingredient ingredient = newIngredient(ingredientID, "Milk", IngredientUnit.ml);
        Ingredient updatedIngredient = newIngredient(ingredientID+1, "Whole milk", IngredientUnit.cup);
        manager.addIngredient(ingredient);
        manager.deleteIngredient(updatedIngredient);

        Ingredient result = manager.findIngredientByID(ingredientID);
        //loaded instance should be equal to the saved one
        assertThat("loaded ingredient differs from the saved one", result, is(equalTo(ingredient)));
        //but it should be another instance
        assertThat("loaded ingredient is the same instance", result, is(not(sameInstance(ingredient))));
        //Grave.equals() method may be broken, check properties' values
        assertDeepEquals(ingredient, result);
    }

    private static Ingredient newIngredient(Long id, String name, IngredientUnit unit) {
        Ingredient ingredient = new Ingredient();
        ingredient.setId(id);
        ingredient.setName(name);
        ingredient.setUnit(unit);
        return ingredient;
    }

    private void assertDeepEquals(List<Ingredient> expectedList, List<Ingredient> actualList) {
        for (int i = 0; i < expectedList.size(); i++) {
            Ingredient expected = expectedList.get(i);
            Ingredient actual = actualList.get(i);
            assertDeepEquals(expected, actual);
        }
    }

    private void assertDeepEquals(Ingredient expected, Ingredient actual) {
        assertEquals("id value is not equal",expected.getId(), actual.getId());
        assertEquals("name value is not equal",expected.getName(), actual.getName());
        assertEquals("unit value is not equal",expected.getUnit(), actual.getUnit());
    }

    private static Comparator<Ingredient> idComparator = new Comparator<Ingredient>() {
        @Override
        public int compare(Ingredient o1, Ingredient o2) {
            return o1.getId().compareTo(o2.getId());
        }
    };

}
