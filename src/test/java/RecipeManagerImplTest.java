import java.sql.*;
import org.junit.Before;
import org.junit.Test;
import javax.sql.DataSource;

import java.util.*;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

/**
 * Tests for RecipeManager class.
 * @author xkopecky
 */
public class RecipeManagerImplTest {

    private RecipeManagerImpl manager;
    private DataSource dataSource;

    @Before
    public void setUp(){
        manager = new RecipeManagerImpl(dataSource);
    }

    // tests for adding
    @Test
    public void addValidRecipe() {
        Long id = 42L;
        List<RecipeCategory> categories = new ArrayList<>();
        categories.add(RecipeCategory.breakfast);

        String instructions = "Cook well";
        String note = "Cook really well";

        Recipe recipe = newRecipe(id, categories, instructions, note);

        Recipe receivedRecipe = manager.findRecipeById(id);
        //loaded instance should be equal to the saved one
        assertThat("Loaded recipe differs from the saved one", receivedRecipe, is(equalTo(recipe)));
        //but it should be another instance
        assertThat("Loaded recipe is the same instance", receivedRecipe, is(not(sameInstance(recipe))));
        //Grave.equals() method may be broken, check properties' values
        assertDeepEquals(recipe, receivedRecipe);

    }

    @Test (expected = IllegalArgumentException.class)
    public void addRecipeWithWrongId() {
        Long id = -42L;

        List<RecipeCategory> categories = new ArrayList<>();
        categories.add(RecipeCategory.breakfast);

        Recipe recipe = newRecipe(id, categories, "Cook well", "Cook really well");
    }

    @Test (expected = IllegalArgumentException.class)
    public void addRecipeWithWrongListOfCategories() {
        Long id = 42L;

        List<RecipeCategory> categories = null;

        String instructions = "Cook well";
        String note = "Cook really well";

        Recipe recipe = newRecipe(id, categories, instructions, note);
    }

    @Test (expected = IllegalArgumentException.class)
    public void addRecipeWithEmptyCategories() {
        Long id = 42L;

        List<RecipeCategory> categories = new ArrayList<>();

        Recipe recipe = newRecipe(id, categories, "Cook well", "Cook really well");
    }


    @Test (expected = IllegalArgumentException.class)
    public void addRecipeWithWrongInstructions() {
        Long id = 42L;

        List<RecipeCategory> categories = new ArrayList<>();
        categories.add(RecipeCategory.breakfast);

        Recipe recipe = newRecipe(id, categories, null, "Cook really well");
    }

    @Test(expected = IllegalArgumentException.class)
    public void addRecipeWithNull() throws Exception {
        manager.addRecipe(null);
    }

    // tests for update
    @Test (expected = IllegalArgumentException.class)
    public void updateRecipeValid() {
        Long id = 42L;
        List<RecipeCategory> categories = new ArrayList<>();
        categories.add(RecipeCategory.breakfast);

        Recipe recipe = newRecipe(id, categories, "Cook well", "Cook really well");
        manager.addRecipe(recipe);

        List<RecipeCategory> updatedCategories = new ArrayList<>();
        categories.add(RecipeCategory.easy);
        Recipe updatedRecipe = newRecipe(id, updatedCategories, "Cook better", "Be the best");

        manager.updateRecipe(updatedRecipe);

        Recipe receivedRecipe = manager.findRecipeById(id);
        //loaded instance should be equal to the saved one
        assertThat("Loaded recipe differs from the saved one", receivedRecipe, is(equalTo(updatedRecipe)));
        //but it should be another instance
        assertThat("Loaded recipe is the same instance", receivedRecipe, is(not(sameInstance(updatedRecipe))));
        //Method equals may be broken, check properties' values
        assertDeepEquals(updatedRecipe, receivedRecipe);

    }

    @Test(expected = IllegalArgumentException.class)
    public void updateRecipeWithEmptyCategories() {
        Long id = 42L;
        List<RecipeCategory> categories = new ArrayList<>();
        categories.add(RecipeCategory.breakfast);

        Recipe recipe = newRecipe(id, categories, "Cook well", "Cook really well");
        manager.addRecipe(recipe);

        List<RecipeCategory> emptyCategories = new ArrayList<>();
        Recipe updatedRecipe = newRecipe(id, emptyCategories, "Cook well", "Cook really well");

        manager.updateRecipe(updatedRecipe);
    }

    @Test(expected = IllegalArgumentException.class)
    public void updateRecipeWithNullCategories() {
        Long id = 42L;
        List<RecipeCategory> categories = new ArrayList<>();
        categories.add(RecipeCategory.breakfast);

        Recipe recipe = newRecipe(id, categories, "Cook well", "Cook really well");
        manager.addRecipe(recipe);

        List<RecipeCategory> nullCategories = null;
        Recipe updatedRecipe = newRecipe(id, nullCategories, "Cook well", "Cook really well");

        manager.updateRecipe(updatedRecipe);
    }

    @Test(expected = IllegalArgumentException.class)
    public void updateRecipeWithWrongInstructions() {
        Long id = 42L;
        List<RecipeCategory> categories = new ArrayList<>();
        categories.add(RecipeCategory.breakfast);

        Recipe recipe = newRecipe(id, categories, "Cook well", "Cook really well");
        manager.addRecipe(recipe);

        Recipe updatedRecipe = newRecipe(id, categories, null, "Cook really well");

        manager.updateRecipe(updatedRecipe);
    }

    @Test(expected = IllegalArgumentException.class)
    public void updateRecipeWithNull() throws Exception {
        manager.updateRecipe(null);
    }
    // tests for delete
    @Test(expected = IllegalArgumentException.class)
    public void deleteRecipeWithWrongInstructions() {
        Long id = 42L;
        List<RecipeCategory> categories = new ArrayList<>();
        categories.add(RecipeCategory.breakfast);

        Recipe recipe = newRecipe(id, categories, "Cook well", "Cook really well");
        manager.addRecipe(recipe);

        Recipe recipeToDeletion = newRecipe(id, categories, null, "Cook really well");

        manager.deleteRecipe(recipeToDeletion);
    }

    @Test(expected = IllegalArgumentException.class)
    public void deleteRecipeWitEmptyStorage() {
        Long id = 42L;
        List<RecipeCategory> categories = new ArrayList<>();
        categories.add(RecipeCategory.breakfast);

        Recipe recipe = newRecipe(id, categories, "Cook well", "Cook really well");
        manager.addRecipe(recipe);

        List<RecipeCategory> emptyCategories = new ArrayList<>();
        Recipe recipeToDeletion = newRecipe(id, emptyCategories, "Cook well", "Cook really well");

        manager.deleteRecipe(recipeToDeletion);
    }

    @Test
    public void deletedRecipeNotInStorage() {
        Long id = 42L;
        List<RecipeCategory> categories = new ArrayList<>();
        categories.add(RecipeCategory.breakfast);

        Recipe recipe = newRecipe(id, categories, "Cook well", "Cook really well");
        manager.addRecipe(recipe);

        List<RecipeCategory> categoriesToDelete = new ArrayList<>();
        categories.add(RecipeCategory.easy);
        Recipe recipeToDeletion = newRecipe(id+1, categoriesToDelete, "Cook pretty well", "Tastes good");

        manager.deleteRecipe(recipeToDeletion);

        Recipe receivedRecipe = manager.findRecipeById(id);
        //loaded instance should be equal to the saved one
        assertThat("Loaded recipe differs from the saved one", receivedRecipe, is(equalTo(recipe)));
        //but it should be another instance
        assertThat("Loaded recipe is the same instance", receivedRecipe, is(not(sameInstance(recipe))));
        //method equals may be broken, check properties' values
        assertDeepEquals(recipe, receivedRecipe);
    }

    @Test(expected = IllegalArgumentException.class)
    public void deleteRecipeWithNull() throws Exception{
        manager.deleteRecipe(null);
    }

    private static Recipe newRecipe(Long id, List<RecipeCategory> categories, String instructions, String note) {
        Recipe recipe = new Recipe();
        recipe.setId(id);
        recipe.setCategories(categories);
        recipe.setInstructions(instructions);
        recipe.setNote(note);

        return recipe;
    }

    //test for find
    @Test
    public void findRecipeByValidId() {
        Long id = 42L;
        List<RecipeCategory> categories = new ArrayList<>();
        categories.add(RecipeCategory.breakfast);

        Recipe recipe = newRecipe(id, categories, "Cook well", "Cook really well");
        manager.addRecipe(recipe);

        Recipe receivedRecipe = manager.findRecipeById(id);
        //loaded instance should be equal to the saved one
        assertThat("Loaded recipe differs from the saved one", receivedRecipe, is(equalTo(recipe)));
        //but it should be another instance
        assertThat("Loaded recipe is the same instance", receivedRecipe, is(not(sameInstance(recipe))));
        //method equals may be broken, check properties' values
        assertDeepEquals(recipe, receivedRecipe);
    }

    @Test
    public void findRecipeNotInCookBook() {
        Long id = 42L;
        List<RecipeCategory> categories = new ArrayList<>();
        categories.add(RecipeCategory.breakfast);

        Recipe recipe = newRecipe(id, categories, "Cook well", "Cook really well");
        manager.addRecipe(recipe);

        Recipe receivedRecipe = manager.findRecipeById(id+1);

        //loaded instance should be equal to the saved one
        assertThat("Loaded recipe differs from the saved one", receivedRecipe, is(not(equalTo(recipe))));
        //but it should be another instance
        assertThat("Loaded recipe is the same instance", receivedRecipe, is(sameInstance(recipe)));
        //method equals may be broken, check properties' values
        assertDeepEquals(recipe, receivedRecipe);

    }

    @Test(expected = IllegalArgumentException.class)
    public void findRecipeWithNull() throws Exception {
        manager.findRecipeById(null);
    }

    // test for findAllRecipes
    @Test
    public void findAllRecipesValid() {
        List<Recipe> listOfTestRecipes = new ArrayList<>();
        Long id = 1L;
        List<RecipeCategory> categories = new ArrayList<>();
        categories.add(RecipeCategory.breakfast);

        Recipe recipe = newRecipe(id, categories, "Cook well", "Cook really well");
        manager.addRecipe(recipe);
        listOfTestRecipes.add(recipe);

        Long id2 = 2L;
        List<RecipeCategory> categories2 = new ArrayList<>();
        categories.add(RecipeCategory.dessert);

        Recipe recipe2 = newRecipe(id2, categories2, "Cook pretty well", "Cook just well");
        manager.addRecipe(recipe2);
        listOfTestRecipes.add(recipe2);

        List<Recipe> listOfRecipes = manager.findAllRecipes();

        assertDeepEquals(listOfTestRecipes, listOfRecipes);
    }

    @Test
    public void findAllRecipesEmptyCookbook() {
        List<Recipe> listOfRecipes = manager.findAllRecipes();
        assertEquals(null, listOfRecipes);
    }

    private void assertDeepEquals(List<Recipe> expectedList, List<Recipe> actualList) {
        for (int i = 0; i < expectedList.size(); i++) {
            Recipe expected = expectedList.get(i);
            Recipe actual = actualList.get(i);
            assertDeepEquals(expected, actual);
        }
    }

    private void assertDeepEquals(Recipe expected, Recipe actual) {
        assertEquals("ID value is not equal",expected.getId(), actual.getId());
        assertEquals("Instructions value is not equal",expected.getInstructions(), actual.getInstructions());
        assertEquals("Note value is not equal",expected.getNote(), actual.getNote());
        assertEquals("Categories value is not equal",expected.getCategories(), actual.getCategories());
    }

    private static Comparator<Recipe> idComparator = new Comparator<Recipe>() {
        @Override
        public int compare(Recipe o1, Recipe o2) {
            return o1.getId().compareTo(o2.getId());
        }
    };

}
