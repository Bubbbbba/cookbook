import java.util.List;

/**
 * Interface for cookbook manager.
 * @author xkopecky, xliberov
 */
public interface CookbookManager {
    /**
     * Method for adding Recipe to cookbook.
     */
    public void addRecipe();

    /**
     * Method that lists all recipes in cookbook.
     */
    public void listAllRecipes();

    /**
     * Method that list all recipes with given string in their name.
     * @param name - string that should be found in recipe name
     * @return list of recipes with given string in their name
     */
    public List<Recipe> findRecipeByName(String name);

    /**
     * Method that lists all recipes in given category.
     * @param category required recipe category
     * @return list of all recipes in given category
     */
    public List<Recipe> findRecipeByCategory(RecipeCategory category);

    /**
     * Method that lits all recipes using given ingredient.
     * @param ingredient required ingredient
     * @return list of all recipes using given ingredient
     */
    public List<Recipe> findRecipeByIngredient (Ingredient ingredient);
}
