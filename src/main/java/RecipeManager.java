import java.util.List;

/**
 * Interface for recipe manager.
 * @author xkopecky, xliberov
 */
public interface RecipeManager {
    /**
     * Method that adds given recipe to used storage.
     * @param recipe recipe that should be added
     */
    public void addRecipe(Recipe recipe);

    /**
     * Method that updates recipe in storage based on its id.
     * @param recipe recipe to be updated
     */
    public void updateRecipe(Recipe recipe);

    /**
     * Method that deletes recipe from storage.
     * @param recipe recipe to be deleted
     */
    public void deleteRecipe(Recipe recipe);

    /**
     * Method that finds recipe by its id.
     * @param id id of recipe
     * @return recipe with given id, null if the recipe does not exist
     */
    public Recipe findRecipeById(Long id);

    /**
     * Method that lists all recipes saved in storage.
     * @return list of all recipes
     */
    public List<Recipe> findAllRecipes();
}
