/**
 * This enum represents units that can be used for ingredient.
 * @author xkopecky, xliberov
 */
public enum IngredientUnit {
    g,
    ml,
    pcs,
    tbls,
    tsp,
    cup
}
