import java.util.List;

/**
 * Interface for ingredient usage manager.
 * @author xkopecky, xliberov
 */
public interface IngredientUsageManager {
    /**
     * Method that adds ingredient usage to used storage.
     * @param usage ingredient usage to be added
     */
    public void addIngredientUsage(IngredientUsage usage);

    /**
     * Method that updates ingredient usage in storage based on its id.
     * @param usage ingredient usage to be updated
     */
    public void updateIngredientUsage(IngredientUsage usage);

    /**
     * Method that deletes ingredient usage from storage.
     * @param usage ingredient usage to be deleted
     */
    public void deleteIngredientUsage(IngredientUsage usage);

    /**
     * Method that finds ingredients for a recipe.
     * @param recipe recipe whose ingredients should be listed
     * @return list of all ingredients used in recipe
     */
    public List<Recipe> findUsedIngredients(Recipe recipe);

    /**
     * Method that finds recipes using given ingredient.
     * @param ingredient ingredient that should be used by recipes
     * @return list of all recipes using the ingredient
     */
    public List<Ingredient> findRecipesUsingIngredient(Ingredient ingredient);

    /**
     * Method that finds ingredient usage by its id.
     * @param id id of ingredient usage
     * @return ingredient usage with given id, null if the ingredeint usage does not exist
     */
    public IngredientUsage findIngredientUsageById(Long id);

    /**
     * Method that lists all ingredient usages saved in storage.
     * @return list of all ingredient usages
     */
    public List<IngredientUsage> findAllIngredientUsages();
}
