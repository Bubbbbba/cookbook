/**
 * This enum represents available categories for recipe.
 * @author xkopecky, xliberov
 */
public enum RecipeCategory {
    breakfast,
    dinner,
    lunch,
    fast,
    easy,
    dessert
}
