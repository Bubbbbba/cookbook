import java.util.List;

/**
 * This entity class represents recipe. Recipe has  unigue id, list of categories, instructions and note.
 * @author xkopecky, xliberov
 */
public class Recipe {
    private Long id;
    private List<RecipeCategory> categories;
    private String instructions;
    private String note;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<RecipeCategory> getCategories() {
        return categories;
    }

    @Override
    public String toString() {
        return "Recipe{" +
                "id=" + id +
                ", categories=" + categories +
                ", instructions='" + instructions + '\'' +
                ", note='" + note + '\'' +
                '}';
    }

    public void setCategories(List<RecipeCategory> categories) {
        this.categories = categories;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Recipe recipe = (Recipe) o;

        if (!id.equals(recipe.id)) return false;
        if (categories != null ? !categories.equals(recipe.categories) : recipe.categories != null) return false;
        if (instructions != null ? !instructions.equals(recipe.instructions) : recipe.instructions != null)
            return false;
        return note != null ? note.equals(recipe.note) : recipe.note == null;

    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + (categories != null ? categories.hashCode() : 0);
        result = 31 * result + (instructions != null ? instructions.hashCode() : 0);
        result = 31 * result + (note != null ? note.hashCode() : 0);
        return result;
    }

    public Recipe(Long ingredientID, List listOfRecipe, String cook, String s) {

    }
    public Recipe() {

    }

    public String getInstructions() {
        return instructions;
    }

    public void setInstructions(String instructions) {
        this.instructions = instructions;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }


}
