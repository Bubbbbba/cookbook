import java.util.List;

/**
 * Class that implements cookbook manager.
 * @author xkopecky, xliberov
 */
public class CookbookManagerImpl implements CookbookManager {
    @Override
    public void addRecipe() {

    }

    @Override
    public void listAllRecipes() {

    }

    @Override
    public List<Recipe> findRecipeByName(String name) {
        return null;
    }

    @Override
    public List<Recipe> findRecipeByCategory(RecipeCategory category) {
        return null;
    }

    @Override
    public List<Recipe> findRecipeByIngredient(Ingredient ingredient) {
        return null;
    }
}
