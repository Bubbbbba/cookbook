import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;

/**
 * Class that implements recipe manager.
 *
 * @author xkopecky, xliberov
 */
public class RecipeManagerImpl implements RecipeManager {

    private final DataSource dataSource;

    public RecipeManagerImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public void addRecipe(Recipe recipe) throws RecipeManagerFailureException {
        validate(recipe);
        if (recipe.getId() != null) {
            throw new IllegalArgumentException("Recipe id is already in use");
        }

        try (Connection connection = dataSource.getConnection();
             PreparedStatement stat = connection.prepareStatement(
                     "INSERT INTO RECIPE (id,categories,instructions,note) VALUES (?,?,?,?)",
                     Statement.RETURN_GENERATED_KEYS)) {
            stat.setLong(1, recipe.getId());
            stat.setObject(2, recipe.getCategories());
            stat.setString(3, recipe.getInstructions());
            stat.setString(4, recipe.getNote());
            int addedRecipe = stat.executeUpdate();
            if (addedRecipe != 1) {
                throw new RecipeManagerFailureException("Internal Error: More recipes ("
                        + addedRecipe + ") added when trying to add recipe " + recipe);
            }
        } catch (SQLException ex) {
            throw new RecipeManagerFailureException("Error when adding recipe " + recipe, ex);
        }
    }

    @Override
    public void updateRecipe(Recipe recipe) throws RecipeManagerFailureException {
        validate(recipe);
        if (recipe.getId() == null) {
            throw new IllegalArgumentException("Recipe is null");
        }

        try (Connection connection = dataSource.getConnection();
             PreparedStatement stat = connection.prepareStatement(
                     "UPDATE Recipe SET id = ?, categories = ?, instructions = ?, note = ? WHERE id = ?")) {

            stat.setLong(1, recipe.getId());
            stat.setObject(2, recipe.getCategories());
            stat.setString(3, recipe.getInstructions());
            stat.setString(4, recipe.getNote());

            int count = stat.executeUpdate();
            if (count == 0) {
                throw new EntityNotFoundException("Recipe " + recipe + " was not found in database!");
            } else if (count != 1) {
                throw new RecipeManagerFailureException("Invalid update count detected (at least one attribute should be changed): " + count);
            }
        } catch (SQLException ex) {
            throw new RecipeManagerFailureException("Error when updating recipe " + recipe, ex);
        }
    }

    @Override
    public void deleteRecipe(Recipe recipe) throws RecipeManagerFailureException {
        validate(recipe);
        if (recipe == null) {
            throw new IllegalArgumentException("Recipe is null");
        }
        if (recipe.getId() == null) {
            throw new IllegalArgumentException("Recipe is null");
        }

        try (Connection connection = dataSource.getConnection();
             PreparedStatement stat = connection.prepareStatement(
                     "DELETE FROM recipe WHERE id = ?")) {

            stat.setLong(1, recipe.getId());

            int count = stat.executeUpdate();
            if (count == 0) {
                throw new EntityNotFoundException("Recipe " + recipe + " was not found in database!");
            } else if (count != 1) {
                throw new RecipeManagerFailureException("Invalid delete count detected (at least one attribute should be changed): " + count);
            }

        } catch (SQLException ex) {
            throw new RecipeManagerFailureException("Error when deleting recipe " + recipe, ex);
        }

    }

    @Override
    public Recipe findRecipeById(Long id) throws RecipeManagerFailureException {
        if (id == null) {
            throw new IllegalArgumentException("Id is null");
        }
        Recipe tmpRecipe;
        try (Connection connection = dataSource.getConnection();
             PreparedStatement stat = connection.prepareStatement(
                     "SELECT FROM recipe WHERE id = ?")) {

            ResultSet rs = stat.executeQuery();
            tmpRecipe = resultSetToRecipe(rs);
            if (tmpRecipe == null) {
                throw new RecipeManagerFailureException("Error when finding recipe ");
            }

        } catch (SQLException ex) {
            throw new RecipeManagerFailureException("Error when finding recipe ", ex);
        }

        return tmpRecipe;
    }

    @Override
    public List<Recipe> findAllRecipes() {

        try (Connection connection = dataSource.getConnection();
             PreparedStatement stat = connection.prepareStatement(
                     "SELECT id,category,instructions,capacity,note FROM recipe")) {

            ResultSet rs = stat.executeQuery();
            List<Recipe> listOfRecipes = new ArrayList<>();

            while (rs.next()) {
                listOfRecipes.add(resultSetToRecipe(rs));
            }
            return listOfRecipes;

        } catch (SQLException ex) {
            throw new RecipeManagerFailureException("Error when finding recipe ", ex);
        }
    }

    private void validate(Recipe recipe) throws IllegalArgumentException {
        if (recipe == null) {
            throw new IllegalArgumentException("Recipe is null");
        }
        if (recipe.getId() < 0) {
            throw new IllegalArgumentException("Recipe id is negative number");
        }
    }

    private Recipe resultSetToRecipe(ResultSet rs) throws SQLException {
        Recipe recipe = new Recipe();
        recipe.setId(rs.getLong("id"));
        recipe.setCategories(rs.getObject("category", ArrayList.class));
        recipe.setInstructions(rs.getString("instructions"));
        recipe.setNote(rs.getString("note"));
        return recipe;
    }

}
