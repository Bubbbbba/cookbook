/**
 * This exception indicates service failure.
 *
 * @author xkopecky
 */
public class RecipeManagerFailureException extends RuntimeException {

    public RecipeManagerFailureException(String msg) {
        super(msg);
    }

    public RecipeManagerFailureException(Throwable cause) {
        super(cause);
    }

    public RecipeManagerFailureException(String message, Throwable cause) {
        super(message, cause);
    }

}