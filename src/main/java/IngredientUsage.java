/**
 * This entity class represents ingredient usage. Usage has  unigue id, recipe, ingredient and its amount.
 * @author xkopecky, xliberov
 */
public class IngredientUsage {
    private Long id;
    private Recipe recipe;
    private Ingredient ingredient;
    private int amoung;

    public Long getId() {
        return id;
    }

    @Override
    public String toString() {
        return "IngredientUsage{" +
                "id=" + id +
                ", recipe=" + recipe +
                ", ingredient=" + ingredient +
                ", amoung=" + amoung +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        IngredientUsage that = (IngredientUsage) o;

        if (amoung != that.amoung) return false;
        if (!id.equals(that.id)) return false;
        if (recipe != null ? !recipe.equals(that.recipe) : that.recipe != null) return false;
        return ingredient != null ? ingredient.equals(that.ingredient) : that.ingredient == null;

    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + (recipe != null ? recipe.hashCode() : 0);
        result = 31 * result + (ingredient != null ? ingredient.hashCode() : 0);
        result = 31 * result + amoung;
        return result;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Ingredient getIngredient() {
        return ingredient;
    }

    public void setIngredient(Ingredient ingredient) {
        this.ingredient = ingredient;
    }

    public Recipe getRecipe() {
        return recipe;
    }

    public void setRecipe(Recipe recipe) {
        this.recipe = recipe;
    }

    public int getAmoung() {
        return amoung;
    }

    public void setAmoung(int amoung) {
        this.amoung = amoung;
    }

    public IngredientUsage() {

    }
}
