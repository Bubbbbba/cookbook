import java.util.List;

/**
 * Class that implements ingredient usage manager.
 * @author xkopecky, xliberov
 */
public class IngredientUsageManagerImpl implements IngredientUsageManager {
    @Override
    public void addIngredientUsage(IngredientUsage usage) {

    }

    @Override
    public void updateIngredientUsage(IngredientUsage usage) {

    }

    @Override
    public void deleteIngredientUsage(IngredientUsage usage) {

    }

    @Override
    public List<Recipe> findUsedIngredients(Recipe recipe) {
        return null;
    }

    @Override
    public List<Ingredient> findRecipesUsingIngredient(Ingredient ingredient) {
        return null;
    }

    @Override
    public IngredientUsage findIngredientUsageById(Long id) {
        return null;
    }

    @Override
    public List<IngredientUsage> findAllIngredientUsages() {
        return null;
    }
}
