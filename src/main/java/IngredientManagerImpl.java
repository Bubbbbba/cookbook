import java.util.List;

/**
 * Class that implements ingredient manager.
 * @author xkopecky, xliberov
 */
public class IngredientManagerImpl implements IngredientManager {
    @Override
    public void addIngredient(Ingredient ingredient) {

    }

    @Override
    public void updateIngredient(Ingredient ingredient) {

    }

    @Override
    public void deleteIngredient(Ingredient ingredient) {

    }

    @Override
    public Ingredient findIngredientByID(Long id) {
        return null;
    }

    @Override
    public List<Ingredient> findAllIngredients() {
        return null;
    }
}
