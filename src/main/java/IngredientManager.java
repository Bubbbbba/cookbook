import java.util.List;

/**
 * Interface for ingredient manager.
 * @author xkopecky, xliberov
 */
public interface IngredientManager {
    /**
     * Method that adds ingredient to used storage.
     * @param ingredient ingredient that should be added
     */
    public void addIngredient(Ingredient ingredient);

    /**
     * Method that updates ingredient in storage based on its id.
     * @param ingredient ingredient that should be updated
     */
    public void updateIngredient(Ingredient ingredient);

    /**
     * Method that deletes ingredient from storage.
     * @param ingredient ingredient that should be deletes
     */
    public void deleteIngredient (Ingredient ingredient);

    /**
     * Method that finds ingredient by its id.
     * @param id id of ingredient
     * @return ingredient with given id, null if the ingredient does not exist
     */
    public Ingredient findIngredientByID(Long id);

    /**
     * Method that lists all ingredients saved in storage.
     * @return list of all ingredients
     */
    public List<Ingredient> findAllIngredients();
}
